<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CloudTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CloudTable Test Case
 */
class CloudTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CloudTable
     */
    protected $Cloud;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Cloud',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Cloud') ? [] : ['className' => CloudTable::class];
        $this->Cloud = TableRegistry::getTableLocator()->get('Cloud', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Cloud);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
