<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cloud $cloud
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $cloud->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $cloud->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Cloud'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="cloud form content">
            <?= $this->Form->create($cloud) ?>
            <fieldset>
                <legend><?= __('Edit Cloud') ?></legend>
                <?php
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
