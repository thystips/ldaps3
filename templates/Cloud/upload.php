<div class="ui center aligned middle aligned grid">
    <div class="row">
        <div class="six wide column">
            <h2 class="ui header">Uploader un fichier</h2>
        </div>
    </div>

    <div class="row">
        <div class="ui six wide column">
            <?= $this->Form->create(null, ['type' => 'file', 'class' => 'ui segment form']) ?>

            <div class="ui field grid center aligned">
                <label for="file" class="ui twelve wide column labeled icon large button">
                    <i class="upload icon"></i>
                    Fichier :
                </label>
                <input type="file" name="file" id="file" class="ui transition hidden">
            </div>

            <?php
            echo $this->Form->submit('Envoyer', ['class' => 'ui button']);
            echo $this->Form->end();
            ?>
        </div>
    </div>

    <div class="row">
        <div class="ui six wide centered column">
            <?= $this->Html->link('Retour au Cloud', ['controller' => 'Cloud', 'action' => 'index'], ['class' => 'ui button']) ?>
        </div>
    </div>

</div>
