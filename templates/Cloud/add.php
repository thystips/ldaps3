<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cloud $cloud
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Cloud'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="cloud form content">
            <?= $this->Form->create($cloud) ?>
            <fieldset>
                <legend><?= __('Add Cloud') ?></legend>
                <?php
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
