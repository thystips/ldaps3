<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cloud[]|\Cake\Collection\CollectionInterface $cloud
 */
?>
<br><br>
<div class="ui center grid">
    <div class="row">
        <div class="ui sixteen wide column">
            <h1 class="ui header center aligned">Cloud de <?= $user["uid"] ?> <?= $this->Html->link('Se déconnecter', ['controller' => 'Users', 'action' => 'logout'], ['class' => 'ui button orange inverted']) ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="ui twelve wide centered column">
            <h3 class="ui header center aligned">Uploader un
                fichier <?= $this->Html->link('Uploader', ['controller' => 'Cloud', 'action' => 'upload'], ['class' => 'ui button']) ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="ui twelve wide centered column">
            <table class="ui very basic celled table">
                <thead>
                <tr>
                    <th>
                        Fichier
                    </th>
                    <th>
                        Taille
                    </th>
                    <th>
                        Date de création
                    </th>
                    <th>
                        Actions
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php if (empty($content)) : ?>

                    <tr>
                        <td colspan="4" class="ui center aligned">
                            <p>Pas encore de fichiers dans le Bucket.</p>
                        </td>
                    </tr>
                <?php else: ?>
                    <tr>
                    <?php foreach ($content as $object) : ?>
                        <td>
                            <?= $object["Key"] ?>
                        </td>
                        <td>
                            <?= $object["Size"] ?> octets
                        </td>
                        <td>
                            <?= $object["LastModified"]->format('d/m/Y') ?>
                        </td>
                        <td>
                            <div class="ui container fluid center aligned">
                                <div class="ui buttons">
                                    <?= $this->Html->link(
                                        'Télécharger',
                                        ['controller' => 'Cloud', 'action' => 'download', '_full' => true, $object["Key"]],
                                        ['class' => 'ui inverted blue button', 'target' => '_blank']
                                    ); ?><?= $this->Html->link(
                                        'Supprimer',
                                        ['controller' => 'Cloud', 'action' => 'remove', '_full' => true, $object["Key"]],
                                        ['class' => 'ui inverted red button', 'target' => '_blank', 'onclick' => 'setTimeout(() => {location.reload()}, 500)']
                                    ); ?>
                                </div>
                            </div>
                        </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
