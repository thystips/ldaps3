<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cloud $cloud
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Cloud'), ['action' => 'edit', $cloud->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Cloud'), ['action' => 'delete', $cloud->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cloud->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Cloud'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Cloud'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="cloud view content">
            <h3><?= h($cloud->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($cloud->id) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
