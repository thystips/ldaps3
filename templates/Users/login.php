<br><br>
<h1 class="ui header centered">Login</h1>
<?php
echo $this->Form->create(null, ['class' => 'ui form centered grid']);
?>
<div class="row">
    <div class="field ui five wide column">
        <label for="username">Nom d'utilisateur :</label>
        <input type="text" name="username" id="username">
    </div>
</div>
<div class="row">
    <div class="field ui five wide column">
        <label for="password">Mot de passe :</label>
        <input type="password" name="password" id="password">
    </div>
</div>
<div class="ui container fluid center aligned">
    <input type="submit" value="Connexion" class="ui button inverted blue">
</div>
<?php
echo $this->Form->end();
?>
