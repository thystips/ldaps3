<?php
/**
 * @var \App\View\AppView $this
 * @var array $params
 * @var string $message
 */
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="ui positive small message" onclick="this.classList.add('hidden')">
    <i class="close icon"></i>
    <div class="header"><?= $message ?></div>
</div>
