<?php

namespace Deployer;

require 'recipe/cakephp.php';

// Project name
set('application', 'ladaps3');

// Project repository
set('repository', 'git@git.ipa.al.local:antoinethys/ladaps3.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);
set('ssh_multiplexing', false);

// Shared files/dirs between deploys
add('shared_files', [
    'config/.env',
    'config/app_local.php',
    'config/app.php'
]);
add('shared_dirs', [
    'logs',
    'tmp'
]);

// Writable dirs by web server
add('writable_dirs', ["tmp"]);

// Hosts

host('app.ipa.al.local')
    ->user('deployment')
    ->set('deploy_path', '/var/www/app');

desc('Deploy your project');
task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:composer',
    'deploy:migrate',
    'deploy:writable',
    'deploy:init',
    'deploy:symlink',
]);

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

task('deploy:composer', function () {
    run('cd {{release_path}} && composer install -n');
    run('cp -rf {{release_path}}/resources/queencitycodefactory {{release_path}}/vendor');
});

task('deploy:migrate', function () {
    run('{{release_path}}/bin/cake migrations migrate');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

