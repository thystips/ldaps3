<?php
declare(strict_types=1);

namespace App\Controller;

use App\Cloud\CloudLDAP;
use Aws\S3\Exception\S3Exception;

/**
 * Cloud Controller
 *
 * @property \App\Model\Table\CloudTable $Cloud
 *
 * @method \App\Model\Entity\Cloud[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CloudController extends AppController
{

    public function initialize(): void
    {
        parent::initialize(); // TODO: Change the autogenerated stub
        $this->loadComponent('Amazon');
    }

    public function isAuthorized($user)
    {

        return false;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $bucket_content = $this->Amazon->listObjectsOfBucket($this->Auth->user()["uid"]);

        $this->set('content', $bucket_content);
        $this->set('user', $this->Auth->user());
    }

    /**
     * View method
     *
     * @param string|null $id Cloud id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cloud = $this->Cloud->get($id, [
            'contain' => [],
        ]);

        $this->set('cloud', $cloud);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cloud = $this->Cloud->newEmptyEntity();
        if ($this->request->is('post')) {
            $cloud = $this->Cloud->patchEntity($cloud, $this->request->getData());
            if ($this->Cloud->save($cloud)) {
                $this->Flash->success(__('The cloud has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cloud could not be saved. Please, try again.'));
        }
        $this->set(compact('cloud'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Cloud id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cloud = $this->Cloud->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cloud = $this->Cloud->patchEntity($cloud, $this->request->getData());
            if ($this->Cloud->save($cloud)) {
                $this->Flash->success(__('The cloud has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cloud could not be saved. Please, try again.'));
        }
        $this->set(compact('cloud'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Cloud id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cloud = $this->Cloud->get($id);
        if ($this->Cloud->delete($cloud)) {
            $this->Flash->success(__('The cloud has been deleted.'));
        } else {
            $this->Flash->error(__('The cloud could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function upload()
    {

        if ($this->request->is('post') && isset($_FILES['file'])) {

            $ext = explode('.', $_FILES['file']['name']);
            $ext = strtolower(end($ext));

            $tmp_name = md5(uniqid());
            $tmp_complete_name = "{$tmp_name}.{$ext}";
            $tmp_storage_path = ROOT . DS . "webroot" . DS . "uploads" . DS . $tmp_complete_name;

            if (move_uploaded_file($_FILES['file']['tmp_name'], $tmp_storage_path)) {

                try {

                    $put = $this->Amazon->s3->putObject([
                        'Bucket' => $this->Auth->user()["uid"],
                        'Key' => $_FILES['file']['name'],
                        'Body' => fopen($tmp_storage_path, 'rb'),
                        'ACL' => 'public-read'
                    ]);

                    unlink($tmp_storage_path);

                    $this->Flash->success("Fichier envoyé !");

                } catch (S3Exception $e) {
                    $this->Flash->error("Something went wrong during the upload ! " . $e);
                }

            } else {
                $this->Flash->error("Something went wrong during the upload !");
            }

        }

    }

    public function download($file)
    {
        $this->Amazon->downloadFileFromBucket($this->Auth->user()["uid"], $file);
    }

    public function remove($file) {
        $this->Amazon->removeFileFromBucket($this->Auth->user()["uid"], $file);
    }
}
